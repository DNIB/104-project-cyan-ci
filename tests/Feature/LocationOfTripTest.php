<?php

namespace Tests\Feature;

use App\Models\Locations;
use App\Models\Trips;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LocationOfTripTest extends TestCase
{
    use RefreshDatabase;

    private $URL = '/trip/location';

    /**
     * A feature test of Class Locations
     *
     * @return void
     */
    public function testExample()
    {
        $this->seed();
        $this->user = User::find(1);
        $this->trip = Trips::find(1);
        $this->location = Locations::find(1);
        $this->location_diff = Locations::find(2);

        $this->createTest();
        //$this->updateTest();
        //$this->deleteTest();
    }

    private function createTest()
    {
        $response = $this->actingAs( $this->user )->call(
            "POST",
            $this->URL,
            [
                "trip_id" => $this->trip->id,
                "location_id" => '999999',
            ]
        );
        $response->assertStatus(400);

        $response = $this->actingAs( $this->user )->call(
            "POST",
            $this->URL,
            [
                "trip_id" => '999999',
                "location_id" => $this->location->id,
            ]
        );
        $response->assertStatus(400);

        $response = $this->actingAs( $this->user )->call(
            "POST",
            $this->URL,
            [
                "trip_id" => $this->trip->id,
                "location_id" => $this->location->id,
            ]
        );
        $response->assertStatus(302);
    }

    private function updateTest()
    {
        ;
    }

    private function deleteTest()
    {
        ;
    }
}
